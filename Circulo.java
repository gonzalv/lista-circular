import java.util.Iterator;
import java.util.Random;

public class Circulo extends ListaCircular {

	public Circulo(int personas) {
		super();
		llenar(personas);
	}

	private void llenar(int numPersonas) {
		for(int i = 0; i < numPersonas; i++) {
			Integer numero = new Integer(i + 1);
			insertar(numero);
		}
	} 

	public Object matar(int n, Iterator it) {
		Object obj = null;
		for(int i = 0; i < n - 2; i++) {
			obj = it.next();
		}
		obj = it.next();
		it.remove();
		return obj;
	}

	public void run(int pasos) {
		Iterator it = elementos();
		int i = 1; 
		while(!quedaUno()) {
			Object muerto = matar(pasos, it);
			System.out.println("Ejecución: " + i + ". Murió: " + muerto.toString());
			i++;
		}
		System.out.println("Sobrevive: " + it.next().toString());
	}

	public void randomRun() {
		Random rand = new Random();
		int pasos;
		Iterator it = elementos();
		int i = 1;
		while(!quedaUno()) {
			pasos = rand.nextInt(20) + 1;
			System.out.println("Saltando " + pasos + " pasos.");
			Object muerto = matar(pasos, it);
			System.out.println("Ejecución: " + i + ". Murió: " + muerto.toString());
			i++;
		}
		System.out.println("Sobrevive: " + it.next().toString());
	}

	public boolean quedaUno() {
		return getSize() == 1;
	}
}